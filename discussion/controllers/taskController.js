//Controllers contains the functions and business logic of our Express JS application
//Meaning all the operations it can do will be placed in this file

//Uses the "require" directive to allow access to the "Task" model which allows us to access methods to perform CRUD operations
//Allows us to use the contents of the "task.js" file in the "models" folder

const Task = require("../models/task");

//Controller function for GETTING ALL THE TASKS
//Defines the functions to be used in the "taskRoutes.js" file and export these functions
module.exports.getAllTasks = () => {
	// The "return" statement, returns the result of the Mongoose method "find" back to the "taskRoute.js" file which invokes this function when the "/tasks" routes is accessed
	//The "then" method is used to wait for the Mongoose "find" method to finish before sending the result back to the route and eventually to the client/Postman

	//model.mongoose method
	return Task.find({}).then(result => {
		// The "return" statement returns the result of the MongoDB query to the "result" parameter defined in the "then" method
		return result;
	});
}

// Controller function for CREATING A TASK
// The request body coming from the client was passed from the "taskRoute.js" file via the "req.body" as an argument and is renamed as a "requestBody" parameter in the controller file
module.exports.createTask = (requestBody) => {

	//Creates a task object based on the Mongoose Model "task"
	let newTask = new Task({
		//Sets the "name" property with the value received from the client
		name: requestBody.name
	});

	// Saves the newly created "newTask" object in the MongoDB database
	// The "then" method waits until the task is stored in the database or an error is encountered before returning a "true" or "false" value back to the client/Postman
	// The "then" method will accept the following 2 arguments:
		//the first parameter will store the result return by the Mongoose save method
		//the second parameter will store the "error" object
	return newTask.save().then((task, error) => {

		// If an error is encountered returns a "false" boolean back to the client/Postman
		if(error){
			console.log(error);
			return false;

		// Save successful, returns the new task object back to the client/Postman
		} else {
			return task;
		}
	});
}

// Controller function for DELETING A TASK
// "taskId" is the URL parameter passed from the "taskRoutes.js" file
// The task id retrieved from the "req.params.id" property coming from the client is renamed as a "taskId" parameter in the controller file
module.exports.deleteTask = (taskID) => {

	// The "findByIdAndRemove" Mongoose method will look for a task with the same id provided from the URL and remove/delete the document from MongoDB
	// The Mongoose method "findByIdAndRemove" method looks for the document using the "_id" field
	return Task.findByIdAndRemove(taskID).then((removeTask, err) => {

		// If an error is encountered returns a "false" boolean back to the client/Postman
		if(err){
			console.log(err);
			return false;

		// Delete successful, returns the removed task object back to the client/Postman
		} else {
			return removeTask;
		}
	})
}

// Controller function for UPDATING A TASK
// The task id retrieved from the "req.params.id" property coming from the client is renamed as a "taskId" parameter in the controller file
// The updates to be applied to the document retrieved from the "req.body" property coming from the client is renamed as "reqBody"
module.exports.updateTask = (taskID, taskName) => {

	// The "findById" Mongoose method will look for a task with the same id provided from the URL
	// The "return" statement, returns the result of the Mongoose method "findById" back to the "taskRoute.js" file which invokes this function 
	return Task.findById(taskID).then((result, err) => {

		// If an error is encountered returns a "false" boolean back to the client/Postman
		if(err){
			console.log(err);
			return false;
		} else {

			// Results of the "findById" method will be stored in the "result" parameter
			// It's "name" property will be reassigned the value of the "name" received from the request
			result.name = taskName.name;

			// Saves the updated object in the MongoDB database
			// The document already exists in the database and was stored in the "result" parameter
			// The "return" statement returns the result of the "save" method to the "then" method chained to the "findById" method which invokes this function
			return result.save().then((updatedTask, saveErr) =>{

				// If an error is encountered returns a "false" boolean back to the client/Postman
				if(saveErr){
					console.log(saveErr)

				// Update successful, returns the updated task object back to the client/Postman
				} else {
					return updatedTask;
				}
			})
		}
	})
}

module.exports.getTask = (taskID) => {
	return Task.findById(taskID).then((result, err) => {
		if(err){
			console.log(err);
			return false;
		} else {
			return result;
		}
	})
}

module.exports.updateStatus = (taskID, taskStatus) => {
	return Task.findById(taskID).then((result, err) => {
		if(err){
			console.log(err);
			return false;
		} else {
			result.status = taskStatus;

			return result.save().then((updatedStatus, saveErr) =>{
				if(saveErr){
					console.log(saveErr)
				} else {
					return updatedStatus;
				}
			})
		}
	})
}